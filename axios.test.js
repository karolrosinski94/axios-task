import axios from "axios";
import { expect } from "chai";

const apiUrl = "https://api.dropboxapi.com";
const token =
    "token";
const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
    },
};

describe("Dropbox API tests", function () {
    it("Test connection - /check/user endpoint should have 200 status", async function () {
        try {
            const response = await axios.post(
                apiUrl + "/2/check/user",
                { query: "test" },
                config
            );
            expect(response.status).to.be.equal(200);
        } catch (error) {
            console.log(error);
            throw error;
        }
    });

    it("Test connection - /check/user endpoint should return correct data", async function () {
        try {
            const response = await axios.post(
                apiUrl + "/2/check/user",
                { query: "test" },
                config
            );
            expect(response.data).to.be.deep.equal({ result: "test" });
        } catch (error) {
            console.log(error);
            throw error;
        }
    });
});
